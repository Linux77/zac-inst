#!/usr/bin/env python
# -*- coding: UTF-8 -*-
''' 
* Copyright 2022 Leonardo de Araújo Lima 
* This program is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or 
* (at your option) any later version. 
* This program is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of 
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNU General Public License for more details.
* You should have received a copy of the GNU General Public License 
* along with this program. If not, see http://www.gnu.org/licenses/

'''




import tkinter as tk
from tkinter import ttk
import gi, os, subprocess, configparser

gi.require_version('Gst', '1.0')
from gi.repository import Gst as gst
from gi.repository import GObject as gobject

class Wndv(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.geometry('280x380')
        self.title('Vídeos ' + parent.svalue)
        self.sayit(str("Vídeos" + parent.svalue))
        self.disc = parent.svalue
        self.fpath = self.disc + "/Vídeos"
        self.lvar = tk.Variable(value=os.listdir(self.fpath))
        
        tk.Listbox(self,name="listdisc",
                listvariable=self.lvar, width=24,
                height=12).pack(expand=True)

        ttk.Button(self,
                text='Visualizar',
                   name = "btnts",
                command=self.showv).pack(expand=True)

        ttk.Button(self,
                text='Sair',
                command=self.destroy).pack(expand=True)

    def execute_unix(self, inputcommand):
        p = subprocess.Popen(inputcommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        return output

    def sayit(self, a):
        c = 'espeak -v pt-br -s150 --punct="<characters>" "%s" 2>>/dev/null' % a
        self.execute_unix(c)
    
    def showv(self):
        self.video = tk.Frame(self, bg='#000000')
        self.video.pack(fill="none", expand=True)
        selection = self.children['listdisc'].curselection()
        self.filename = self.fpath + "/" + self.children['listdisc'].get(selection)
        print(self.filename)
        gst.init(None)
        player = gst.ElementFactory.make('playbin', 'player')
        player.set_property('video-sink', None)
        player.set_property('force-aspect-ratio', True)
        player.set_property('uri', 'file://%s' % (os.path.abspath(self.filename)))
        self.sayit(self.children['listdisc'].get(selection))
        player.set_state(gst.State.PLAYING)
        bus = player.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()        

class Wndt(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.geometry('380x360')
        self.title('Textos ' + parent.svalue)
        self.sayit(str("Textos" + parent.svalue))
        self.disc = parent.svalue
        self.fpath = self.disc + "/txts"
        self.lvar = tk.Variable(value=os.listdir(self.fpath))
        
        tk.Listbox(self,name="listdisc",
                listvariable=self.lvar, width=24,
                height=12).pack(expand=True)

        ttk.Button(self,
                text='Visualizar',
                   name = "btnts",
                command=self.showf).pack(padx=100,expand=True)


        ttk.Button(self,
                text='Sair',
                   name = 'btnexit',
                command=self.destroy).pack(expand=True)

    def showf(self):
        self.geometry('640x485')
        self.txtfile = tk.Text(self, width=65, height= 22)
        self.children['listdisc'].pack_forget()
        self.children['btnts'].pack_forget()
        self.children['btnexit'].pack_forget()
        selection = self.children['listdisc'].curselection()
        self.filename = self.fpath + "/" + self.children['listdisc'].get(selection)
        self.sayit(self.children['listdisc'].get(selection))

        with open(self.filename) as f:
            self.txtfile.insert(1.0,f.read())
            self.txtfile.pack(fill="none", expand=True)
            btnsay = tk.Button(self,text="Ler",command=self.saytext).pack(expand=True)
            btnexit = tk.Button(self,text="Sair",command=self.destroy).pack(side=tk.TOP, expand=True)

    def execute_unix(self, inputcommand):
        p = subprocess.Popen(inputcommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        return output

    def sayit(self, a):
        c = 'espeak -v pt-br -k5 -s150 --punct="<characters>" "%s" 2>>/dev/null' % a
        self.execute_unix(c)

    def saytext(self):
        text = self.txtfile.get(1.0, 'end')
        self.sayit(text)

#
class Wndst(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.geometry('280x380')
        self.title(parent.svalue)
        self.sayit(str(parent.svalue))
        with open(parent.svalue) as f:
            contents = f.read()
            print(contents)
            
        tk.Textbox(self,name="stxt",
                width=24,
                height=12).pack(expand=True)

        ttk.Button(self,
                   text='Ler',
                   command=self.saytext).pack(expand=True)

        ttk.Button(self,
                text='Sair',
                command=self.destroy).pack(expand=True)

    def execute_unix(self, inputcommand):
        p = subprocess.Popen(inputcommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        return output

    def sayit(self, a):
        c = 'espeak -v pt-br -k5 -s150 --punct="<characters>" "%s" 2>>/dev/null' % a
        self.execute_unix(c)
            
class Wndsv(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.geometry('640x480')
        self.title(parent.svalue)
        self.sayit(str(parent.svalue))
        self.title(parent.svalue)
        self.sayit(str(parent.svalue))
        self.disc = parent.svalue
        self.fpath = self.disc + "/Vídeos"

        ttk.Button(self,
                text='Sair',
                   name = 'btnexit',
                command=self.destroy).pack(expand=True)


    def execute_unix(self, inputcommand):
        p = subprocess.Popen(inputcommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        return output

    def sayit(self, a):
        c = 'espeak -v pt-br -k5 -s150 --punct="<characters>" "%s" 2>>/dev/null' % a
        self.execute_unix(c)

#
        
class Zac(tk.Tk):
    def __init__(self):
        super().__init__()
        config = configparser.ConfigParser()
        config.read(os.getcwd() +'/zac.conf')
        disciplinas = config.get('Curso' , 'disciplinas[]' )
        self.lvar = tk.Variable(value=disciplinas)
        self.geometry('360x280')
        self.title('Zac ambiente de estudos')
        self.svalue = ""
        # place a button on the root window
        tk.Listbox(self, name="listdisc",
                listvariable=self.lvar, width=26,
                height=8).pack(expand=True)


        ttk.Button(self,
                text="Textos",
                command=self.show_texts).pack(expand=True)
        ttk.Button(self,
                text='Vídeos',
                command=self.show_videos).pack(expand=True)
        ttk.Button(self,
                text='Sair',
                command=self.destroy).pack(expand=True)

    def show_texts(self):
        selection = self.children['listdisc'].curselection()
        self.svalue = self.children['listdisc'].get(selection)
        window = Wndt(self)
        window.grab_set()
        window.children['listdisc'].select_set(0)

    def show_videos(self):
        
        selection = self.children['listdisc'].curselection()
        self.svalue = self.children['listdisc'].get(selection)
        window = Wndv(self)
        window.grab_set()
        window.children['listdisc'].select_set(0)

    def play_video(self):
        window = Wndsv(self)
        window.grab_set()
        
    def read_texts(self):
        window = Wndst(self)
        window.grab_set()

    def execute_unix(self, inputcommand):
        p = subprocess.Popen(inputcommand, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        return output

    def sayit(self, a):
        c = 'espeak -v pt-br -k5 -s150 --punct="<characters>" "%s" 2>>/dev/null' % a
        self.execute_unix(c)
        

if __name__ == "__main__":
    app = Zac()
    app.sayit("Iniciando o instrutor virtual!")
    app.children['listdisc'].select_set(0)
    app.mainloop()
