# Zac, virtual instructor.


![Logo](img/Zac-logo-s.png "zac instructor")

This software try help, classroom students and various teachers to produce courses.

My intention is give to teacher one way to display media and text contents using various resources and formats/types.

The project use some libraries and subsystems, and much of these resources are presents in Debian **GNU/Linux** and much others.

**Dependencies.**

> I don't build at the moment one install script. Excuse me.

> The software use some objects naturals on gnome environment.

> Other resources needed is:

> **espeak** text to espeak support with brazilian portuguese or other voices.

> To install that, on debian and debian based systems you only need do that:

sudo apt update

sudo apt install git espeak mbrola-br1 mbrola-br2 mbrola-br3 mbrola-br4

> after this you already have tts support used by zac virtual instructor.

## **Usage.**

To use zac you need follow small rules.

* **first.**

> Create one directory for each content that you can use. Like

> __Math__

> __Physics__

On that you need at least two subdirectories:

txts __location for text files.__

Vídeos __location for vídeo files.__

* **second.**

You need setup zac.conf file, that file setup the content tittles and directory names where you use to store data.

The syntax is very simple.

You use some like:

**zac.conf**

disciplinas[] = Math Physics

After that zac executable can be used inside you **GUI.**

> Text files are listed inside gui window and can be viewed with tts interface option.

> Vídeo files are listed and you can see that.

> These software, can help on various studies and content production for teach.

> The project can have more functions, but already are is usable.

> Some directories here and files are for sampĺes of usage and software work, this can be excluded or replaced.

> zac-vídeos.xges is a pre formated template can be used with "pitivi" on debian to format vídeos on good resolution to be used.



Sorry for my actual limitations, I try made it better on new releases.


---
[**Website for my courses and studies.**](http://www.asl-sl.com.br)
> Here I provide some studies, projects related to tecnology. I teach in my small city during some time an>

> On my courses and studies **website** I use __portuguese,__ my natural language.


[**Website search engine projects:**](http://magicbyte.tec.br:8888/)


> Here I install one public seaech __engine__ for studies end learning purposes.

> The software used, also promotes one metasearch, and small crawler.

> You are welcome to known and colaborate.

_contact_**/contato:**

### Leonardo.

> Apaixonado por tecnologia, professor com graduação em ensino fundamental e médio.

> Desde menino ainda na década de 80 sempre acompanhando evoluções em telecomunicações.

> Radio amador quando jovem, ainda me lembro de antever tecnologias, em nossos ideiais.

> Programador com experiência em linguagens de alto e baixo nível....

> Tive influências de conhecidos e parentes dobre questões de conhcer ambientes na época distantes da maior parte das pessoas.

> ainda me recordo de uma _tia_, programadora de ambientes UNIX, na época de locação de mainframes, me despertou o interesse por me aprofundar em tais estudos.

> Muitos anos depois tive meus primeiros contatos com ambiente BSD's e Linux o que despertou uma paixão adormecida.

> Hoje tenho mais de 20 anos de ensino técnico em uma estrutura prórpia, a qual em minha pequena cidade e região mostrou eficácia, mudando a vida de muitos adolescentees hoje profissionais espealhados por várias localidades.



**fone:** (35) 99120-5702.

> [email](mailto:leonardo@asl-sl.com.br)

> **Linux77.**

### Mônica.

**fone:** (35) 99853-7574.

**email:** [Mônica](mailto:monijucodoro@gmail.com)

[Academia do software livre **Brasil - MG**](http://www.asl-sl.com.br)

[Courses and studies**](http://www.cursos.asl-sl.com.br)

[Um pouco sobre nossa cidade!](http://www.asl-br.com/TMB)

[A short about our City!](http://www.asl-br.com/TMB)

[contact](mailto:feraleomg@gmail.com)

